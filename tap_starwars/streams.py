"""Stream type classes for tap-starwars."""

from __future__ import annotations

import typing as t
from pathlib import Path

from singer_sdk import typing as th  # JSON Schema typing helpers

from tap_starwars.client import StarWarsStream

# SCHEMAS_DIR = Path(__file__).parent / Path("./schemas")


class PeopleStream(StarWarsStream):
    """Define custom stream."""

    name = "people"
    path = "/people"
    primary_keys: t.ClassVar[list[str]] = ["id"]
    replication_key = None
    # Optionally, you may also use `schema_filepath` in place of `schema`:
    # schema_filepath = SCHEMAS_DIR / "users.json"  # noqa: ERA001
    schema = th.PropertiesList(
        th.Property(
            "name",
            th.StringType,
            description="The character's name.",
        ),
        th.Property(
            "birth_year",
            th.StringType,
            description="The character's birth year.",
        ),
        th.Property(
            "gender",
            th.StringType,
            description="The character's gender",
        ),
    ).to_dict()
